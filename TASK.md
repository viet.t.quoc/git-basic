# Hướng dẫn sử dụng git khi được giao task

## Xác định nhánh chung
- Xác định nhánh nào là nhánh chung của developer
- Checkout qua nhánh chung `git checkout [remote] [branch]`

## Tạo branch để bắt đầu task
- Để hạn chế việc bị conflict sau này khi merge vào nhánh chung, ta nên pull code mới nhất của nhánh chung về `git pull [remote] [branch]`
- Chọn tên branch mới, nên chọn tên branch liên quan đến task dự định làm. Ví dụ: `task100_fix_bug_dang_nhap`
- Tạo branch mới để bắt đầu làm `git checkout -b [branch]`

## Commit
- Nếu task phức tạp, sau khi code xong một chức năng cụ thể thì ta nên tạo một commit các file của chức năng đó.
- Không nên add các file chưa code xong vào commit của một chức năng đã code xong.
- Khi hết giờ làm, nếu có các đoạn code phức tạp thì tạo commit cho an toàn. Còn code bình thường thì không nên tạo commit, để hôm sau làm tiếp cũng được.
- Hạn chế tạo commit với comment không rõ ràng.

## Sau khi code xong task
- Kiểm tra lại vài lần để chắc chắn code hoạt động chính xác.
- Commit lần cuối có comment kèm từ khóa `done`, `finish`, `hoàn thành`

## Push và tạo merge request
- Cần merge code của nhánh hiện tại với nhánh chung tại local trước khi tạo merge request.
- Merge code nhánh hiện tại với code mới nhất của nhánh chung `git pull [remote] [nhánh chung]`
- Fix conflict nếu có.
- Push code đã được merge với nhánh chung lên cloud.
- Tạo merge request trên cloud.
- Thông báo với người quản lý.
